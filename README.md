# Developer Setup Guide

This guide assumes you're familiar with Git, if not; go learn.

To proceed you'll need to have Git Bash open, which you can get from installing Git for Windows.
Hereon assumes you proceeded with the default installation options.

Open the folder where'd you'd like to clone the developer repositories, right click in the folder and press
'Open in Git Bash'. 

As the game and content repositories use Git Large File Storage (which is used to store
large files more efficiently), you'll need to have that installed, if you've not installed it bofore, run:

`git lfs install`

Clone the appropriate repositories for the source code, game build and asset sources by running the commands:

```
git clone https://gitlab.com/adv-software/vance-src src
git clone https://gitlab.com/adv-software/vance-game game
git clone https://gitlab.com/adv-software/vance-content content
```

You MUST use these commands to clone the repositories into their respective src, game and content folders. These folder names are important to the workflow topology.

Then configure Git LFS file locking (which is used to prevent from accidentally
pushing to the repositority while somebody else is) for each of the repositories:
```
git config lfs.https://gitlab.com/adv-software/vance-game.git/info/lfs.locksverify true
git config lfs.https://gitlab.com/adv-software/vance-content.git/info/lfs.locksverify true
```
